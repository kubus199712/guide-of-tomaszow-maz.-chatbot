import aiml
from unidecode import unidecode

def remove_diacritic(text):
    return unidecode(text)

kernel = aiml.Kernel()
kernel.learn("data.xml")
kernel.respond("load aiml b")

while True:
    input_text = input(">Ty:  ")
    input_text = remove_diacritic(input_text)
    response = kernel.respond(input_text)
    x = response.split('-k')
    print(">Bot:", x[0])
    for i in x[1:]:
        print("    ", i)
    if response == "Do widzenia :)":
        break
